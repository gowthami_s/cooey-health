package com.siddi.cooeyhealth.sharepref;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.siddi.cooeyhealth.model.Profile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserSession {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    public static final List<Profile> AcceptedProfilesList = new ArrayList<>();

    public static final String PREFER_NAME = "COOEYHEALTH";
    public static final String KEY_UserName = "name";
    public static final String KEY_UserDOB = "dob";
    public static final String KEY_UserGender = "gender";
    public static final String KEY_UserImage = "image";

    // Constructor
    @SuppressLint("CommitPrefEdits")
    public UserSession(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void StoreUserDetails(String name, String dob, String gender, String profile_image) {

        editor.putString(KEY_UserName, name);
        editor.putString(KEY_UserDOB, dob);
        editor.putString(KEY_UserGender, gender);
        editor.putString(KEY_UserImage, profile_image);

        editor.commit();
    }

    public HashMap<String, String> getUserDetails() {

        HashMap<String, String> user = new HashMap<String, String>();

        user.put(KEY_UserName, pref.getString(KEY_UserName, null));
        user.put(KEY_UserDOB, pref.getString(KEY_UserDOB, null));
        user.put(KEY_UserGender, pref.getString(KEY_UserGender, null));
        user.put(KEY_UserImage, pref.getString(KEY_UserImage, null));

        return user;
    }

    public void AcceptedProfilesList(Profile profile) {
        AcceptedProfilesList.add(profile);
    }

    public List<Profile> getAcceptedProfilesList() {
        return AcceptedProfilesList;
    }

    public void deleteAcceptedProfile(Profile mProfile) {
        for (Profile profile : AcceptedProfilesList) {
            if (profile.getEmail().equalsIgnoreCase(mProfile.getEmail())) {
                AcceptedProfilesList.remove(mProfile);
                break;
            }
        }
    }
}
