package com.siddi.cooeyhealth.viewmodels;


import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.siddi.cooeyhealth.BR;
import com.siddi.cooeyhealth.model.User;

public class SignUpViewModel extends BaseObservable {
    private User user;


    private String successMessage = "Login was successful";
    private String errorMessage = "User details are not valid";

    @Bindable
    private String toastMessage = null;


    public String getToastMessage() {
        return toastMessage;
    }


    private void setToastMessage(String toastMessage) {

        this.toastMessage = toastMessage;
        notifyPropertyChanged(BR.toastMessage);
    }


    public void setUserName(String name) {
        user.setName(name);
        notifyPropertyChanged(BR.userName);
    }
    @Bindable
    public String getUserName() {
        return user.getName();
    }


    @Bindable
    public String getUserDOB() {
        return user.getDob();
    }
    public void setUserDOB(String dob) {
        user.setDob(dob);
        notifyPropertyChanged(BR.userDOB);
    }

    public void onSelectItem(AdapterView<?> parent, View view, int pos, long id)
    {
        //pos                                 get selected item position
        //view.getText()                      get lable of selected item
        //parent.getAdapter().getItem(pos)    get item by pos
        //parent.getAdapter().getCount()      get item count
        //parent.getCount()                   get item count
        String selectedGender = parent.getSelectedItem().toString();             //get selected item
        user.setGender(selectedGender);
        notifyPropertyChanged(BR.userGender);
    }

    public void setUserGender(String gender) {
        user.setGender(gender);
        notifyPropertyChanged(BR.userGender);
    }
    @Bindable
    public String getUserGender() {
        return user.getGender();
    }

    @Bindable
    public String getUserImage() {
        return user.getUserImage();
    }

    public void setUserImage(String image) {
        user.setUserImage(image);
        notifyPropertyChanged(BR.userImage);
    }

    public SignUpViewModel() {
        user = new User("","","","");
    }

    public void onLoginClicked() {
        if (isInputDataValid())
            setToastMessage(successMessage);
        else
            setToastMessage(errorMessage);
    }

    public boolean isInputDataValid() {
        return !TextUtils.isEmpty(getUserName()) && !TextUtils.isEmpty(getUserDOB()) && !TextUtils.isEmpty(getUserGender());
    }

    public User getUser() {
        return user;
    }
}