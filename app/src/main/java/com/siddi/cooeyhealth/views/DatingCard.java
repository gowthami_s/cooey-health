package com.siddi.cooeyhealth.views;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;
import com.siddi.cooeyhealth.R;
import com.siddi.cooeyhealth.model.Profile;
import com.siddi.cooeyhealth.sharepref.UserSession;
import com.siddi.cooeyhealth.ui.home.HomeFragment;

@Layout(R.layout.dating_card_view)
public class DatingCard {

    @View(R.id.profileImageView)
    private ImageView profileImageView;

    @View(R.id.nameAgeTxt)
    private TextView nameAgeTxt;

    @View(R.id.locationNameTxt)
    private TextView locationNameTxt;

    @View(R.id.rejectBtn)
    private ImageButton rejectBtn;

    @View(R.id.acceptBtn)
    private ImageButton acceptBtn;

    private Profile mProfile;
    private Context mContext;
    private SwipePlaceHolderView mSwipeView;

    UserSession userSession;
    boolean isLikedProfileSaved;
    private static int redoCount = 0;
    private static int UndoCount = 0;

    public DatingCard(Context context, Profile profile, SwipePlaceHolderView swipeView, boolean isHomeFragment) {
        mContext = context;
        mProfile = profile;
        mSwipeView = swipeView;
        isLikedProfileSaved = isHomeFragment;
        userSession = new UserSession(context);
    }

    public void DatingCard(Context context, Profile profile, SwipePlaceHolderView swipeView) {
        mContext = context;
        mProfile = profile;
        mSwipeView = swipeView;
        userSession = new UserSession(context);
    }

    @Resolve
    private void onResolved(){
        Glide.with(mContext).load(mProfile.getImageUrl()).placeholder(R.mipmap.ic_launcher).dontAnimate().into(profileImageView);
        nameAgeTxt.setText(mProfile.getName() + ", " + mProfile.getAge());
        locationNameTxt.setText(mProfile.getEmail());

        rejectBtn.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                if (redoCount < 3) {
                    redoCount = redoCount + 1;
                    //Toast.makeText(mContext, redoCount + "", Toast.LENGTH_SHORT).show();
                    mSwipeView.doSwipe(false);
                } else {
                    redoCount = 0;
                    Intent i = new Intent(mContext, PaymentActivity.class);
                    mContext.startActivity(i);
                }
            }
        });

        acceptBtn.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                if (UndoCount < 3) {
                    UndoCount = UndoCount + 1;
                    //Toast.makeText(mContext, UndoCount + "", Toast.LENGTH_SHORT).show();
                    mSwipeView.doSwipe(true);
                } else {
                    UndoCount = 0;
                    Intent i = new Intent(mContext, PaymentActivity.class);
                    mContext.startActivity(i);
                }
            }
        });

    }

    @SwipeOut
    private void onSwipedOut(){
        Log.d("EVENT", "onSwipedOut");
        mSwipeView.addView(this);
    }

    @SwipeCancelState
    private void onSwipeCancelState(){
        Log.d("EVENT", "onSwipeCancelState");
    }

    @SwipeIn
    private void onSwipeIn(){
        Log.d("EVENT", "onSwipedIn");
        if (isLikedProfileSaved)
            userSession.AcceptedProfilesList(mProfile);
    }

    @SwipeInState
    private void onSwipeInState(){
        Log.d("EVENT", "onSwipeInState");
    }

    @SwipeOutState
    private void onSwipeOutState(){
        Log.d("EVENT", "onSwipeOutState");

        if (!isLikedProfileSaved)
            userSession.deleteAcceptedProfile(mProfile);
    }

    private void onUndoClick() {
        mSwipeView.undoLastSwipe();
    }
}
