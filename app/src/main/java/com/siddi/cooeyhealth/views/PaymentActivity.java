package com.siddi.cooeyhealth.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.siddi.cooeyhealth.R;

public class PaymentActivity extends AppCompatActivity {

    Button pay_now_btn;
    EditText et_cvv, et_expire_date, et_card_number;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        pay_now_btn = findViewById(R.id.pay_now_btn);
        et_card_number = findViewById(R.id.et_card_number);
        et_expire_date = findViewById(R.id.et_expire_date);
        et_cvv = findViewById(R.id.et_cvv);

        pay_now_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validQuery()) {
                    Toast.makeText(PaymentActivity.this, "Payment Successful", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(PaymentActivity.this, ProfilesActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private boolean validQuery() {
        boolean validate = false;

        if (et_cvv.getText() == null || et_cvv.getText().length() == 0)
            et_cvv.setError("cvv invalid");
        if (et_expire_date.getText() == null || et_expire_date.getText().length() == 0)
            et_expire_date.setError("Expiry date invalid");
        if (et_card_number.getText() == null || et_card_number.getText().length() == 0)
            et_card_number.setError("card number invalid");
        else
            validate = true;

        return validate;
    }
}