package com.siddi.cooeyhealth.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;

import com.siddi.cooeyhealth.R;
import com.siddi.cooeyhealth.databinding.ActivitySignupBinding;
import com.siddi.cooeyhealth.model.User;
import com.siddi.cooeyhealth.sharepref.UserSession;
import com.siddi.cooeyhealth.viewmodels.SignUpViewModel;

public class SignUpActivity  extends AppCompatActivity {

    private static SignUpViewModel viewModel = new SignUpViewModel();
    private static UserSession userSession;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivitySignupBinding activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        activityMainBinding.setViewModel(viewModel);
        activityMainBinding.executePendingBindings();

        userSession = new UserSession(this);
    }

    @BindingAdapter({"toastMessage"})
    public static void runMe(View view, String message) {
        if (message != null && message.contains("successful")) {
            //save user details and move to profile activity
            new SignUpActivity().saveUser();
            view.getContext().startActivity(new Intent(view.getContext(), ProfilesActivity.class));
        }

        if (message != null && message.length() > 0)
            Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
    }

    //save user in shared prefs
    private void saveUser() {
        User user = viewModel.getUser();
        userSession.StoreUserDetails(user.getName(), user.getDob(), user.getGender(), user.getUserImage());
    }
}
