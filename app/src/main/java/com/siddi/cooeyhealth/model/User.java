package com.siddi.cooeyhealth.model;

public class User {
    private String name;
    private String dob;
    private String gender;
    private String userImage;

    public User(String name, String dob, String gender, String userImage) {
        this.name = name;
        this.dob = dob;
        this.gender = gender;
        this.userImage = userImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }
}