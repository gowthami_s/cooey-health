package com.siddi.cooeyhealth.ui.notifications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.siddi.cooeyhealth.R;
import com.siddi.cooeyhealth.data.Utils;
import com.siddi.cooeyhealth.model.Profile;
import com.siddi.cooeyhealth.sharepref.UserSession;
import com.siddi.cooeyhealth.views.DatingCard;

import java.util.List;

public class NotificationsFragment extends Fragment {

    private NotificationsViewModel notificationsViewModel;
    private SwipePlaceHolderView mSwipeView;
    //LinearLayout undo_redo_container;
    TextView tv_no_profiles;
    //ImageButton undoBtn, redoBtn;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                ViewModelProviders.of(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        /*final TextView textView = root.findViewById(R.id.text_notifications);
        notificationsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        mSwipeView = (SwipePlaceHolderView) root.findViewById(R.id.swipeView);
        //undo_redo_container = root.findViewById(R.id.undo_redo_container);
        tv_no_profiles = root.findViewById(R.id.tv_no_profiles);
        /*undoBtn = root.findViewById(R.id.rejectBtn);
        redoBtn = root.findViewById(R.id.acceptBtn);*/

        mSwipeView.getBuilder()
                .setDisplayViewCount(3)
                .setIsUndoEnabled(true)
                .setSwipeDecor(new SwipeDecor()
                        .setPaddingTop(20)
                        .setRelativeScale(0.01f)
                        .setSwipeInMsgLayoutId(R.layout.dating_swipe_in_msg_view)
                        .setSwipeOutMsgLayoutId(R.layout.dating_swipe_out_msg_view));

        List<Profile> likedProfiles = new UserSession(getContext()).getAcceptedProfilesList();

        if (likedProfiles != null && likedProfiles.size() > 0) {
            //undo_redo_container.setVisibility(View.VISIBLE);
            tv_no_profiles.setVisibility(View.INVISIBLE);
            for (Profile profile : likedProfiles) {
                mSwipeView.addView(new DatingCard(getContext(), profile, mSwipeView, false));
            }
        } else {
            //undo_redo_container.setVisibility(View.INVISIBLE);
            tv_no_profiles.setVisibility(View.VISIBLE);
        }

        /*undoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "undo", Toast.LENGTH_SHORT).show();
            }
        });

        redoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "redo", Toast.LENGTH_SHORT).show();
            }
        });*/

        return root;
    }
}