package com.siddi.cooeyhealth.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.siddi.cooeyhealth.R;
import com.siddi.cooeyhealth.sharepref.UserSession;

import static com.siddi.cooeyhealth.sharepref.UserSession.KEY_UserDOB;
import static com.siddi.cooeyhealth.sharepref.UserSession.KEY_UserGender;
import static com.siddi.cooeyhealth.sharepref.UserSession.KEY_UserImage;
import static com.siddi.cooeyhealth.sharepref.UserSession.KEY_UserName;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    UserSession userSession;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        final EditText tv_name = root.findViewById(R.id.user_name);
        final EditText tv_gender = root.findViewById(R.id.user_gender);
        final EditText tv_age = root.findViewById(R.id.user_age);

        final ImageView user_img = root.findViewById(R.id.user_img);

        /*dashboardViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        userSession = new UserSession(getContext());
        String gender = userSession.getUserDetails().get(KEY_UserGender);
        if (userSession.getUserDetails().get(KEY_UserName) != null)
            tv_name.setText(userSession.getUserDetails().get(KEY_UserName));
        if (gender != null)
            tv_gender.setText(gender);
        if (userSession.getUserDetails().get(KEY_UserDOB) != null)
            tv_age.setText(userSession.getUserDetails().get(KEY_UserDOB));

        // for image in profile screen
        if (userSession.getUserDetails().get(KEY_UserImage) != null && userSession.getUserDetails().get(KEY_UserImage).length() > 0)
            Glide.with(getActivity()).load(KEY_UserImage).placeholder(R.mipmap.ic_launcher).dontAnimate().into(user_img);
        else {
            if (gender != null && gender.equalsIgnoreCase("male"))
                Glide.with(getActivity()).load(R.drawable.male_person).placeholder(R.mipmap.ic_launcher).dontAnimate().into(user_img);
            else
                Glide.with(getActivity()).load(R.drawable.female_person).placeholder(R.mipmap.ic_launcher).dontAnimate().into(user_img);
        }

        return root;
    }
}