package com.siddi.cooeyhealth.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.siddi.cooeyhealth.R;
import com.siddi.cooeyhealth.data.Utils;
import com.siddi.cooeyhealth.model.Profile;
import com.siddi.cooeyhealth.views.DatingCard;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private SwipePlaceHolderView mSwipeView;
    private boolean isHomeFragment = true;
    //ImageButton undoBtn, redoBtn;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        //final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });

        mSwipeView = (SwipePlaceHolderView) root.findViewById(R.id.swipeView);
        //undoBtn = root.findViewById(R.id.rejectBtn);
        //redoBtn = root.findViewById(R.id.acceptBtn);

        mSwipeView.getBuilder()
                .setDisplayViewCount(3)
                .setSwipeDecor(new SwipeDecor()
                        .setPaddingTop(20)
                        .setRelativeScale(0.01f)
                        .setSwipeInMsgLayoutId(R.layout.dating_swipe_in_msg_view)
                        .setSwipeOutMsgLayoutId(R.layout.dating_swipe_out_msg_view));


        for(Profile profile : Utils.loadProfiles(getContext())){
            mSwipeView.addView(new DatingCard(getContext(), profile, mSwipeView, isHomeFragment));
        }

        /*undoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "undo", Toast.LENGTH_SHORT).show();
            }
        });

        redoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "redo", Toast.LENGTH_SHORT).show();
            }
        });*/

        return root;
    }
}